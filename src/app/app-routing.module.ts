import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateUserComponent } from './create-user/create-user.component';
import { LoginUserComponent } from './login-user/login-user.component';
import { UserHomeComponent } from './user-home/user-home.component';

const routes: Routes = [
  {path: '', redirectTo: 'create-user',pathMatch: 'full'},
  {path: 'create-user',component: CreateUserComponent},
  {path: 'login-user',component:LoginUserComponent},
  {path: 'user-home',component:UserHomeComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
